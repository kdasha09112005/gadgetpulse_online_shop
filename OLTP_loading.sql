DROP FUNCTION IF EXISTS load_data_from_csv CASCADE;

-- Function to load data from CSV files
CREATE OR REPLACE FUNCTION load_data_from_csv(
    person_file_path TEXT,
    order_file_path TEXT
)
RETURNS VOID AS $$
BEGIN
    -- Ensure unique constraints on necessary tables
    BEGIN
        ALTER TABLE Country ADD CONSTRAINT country_name_unique UNIQUE (name);
    EXCEPTION
        WHEN duplicate_table THEN NULL;
    END;

    BEGIN
        ALTER TABLE Person ADD CONSTRAINT person_email_unique UNIQUE (email);
    EXCEPTION
        WHEN duplicate_table THEN NULL;
    END;

    BEGIN
        ALTER TABLE "Role" ADD CONSTRAINT role_name_unique UNIQUE (name);
    EXCEPTION
        WHEN duplicate_table THEN NULL;
    END;

    BEGIN
        ALTER TABLE Category ADD CONSTRAINT category_name_unique UNIQUE (name);
    EXCEPTION
        WHEN duplicate_table THEN NULL;
    END;

    BEGIN
        ALTER TABLE PaymentMethod ADD CONSTRAINT payment_method_name_unique UNIQUE (name);
    EXCEPTION
        WHEN duplicate_table THEN NULL;
    END;

    BEGIN
        ALTER TABLE DeliveryMethod ADD CONSTRAINT delivery_method_name_unique UNIQUE (name);
    EXCEPTION
        WHEN duplicate_table THEN NULL;
    END;

    BEGIN
        ALTER TABLE Status ADD CONSTRAINT status_name_unique UNIQUE (name);
    EXCEPTION
        WHEN duplicate_table THEN NULL;
    END;

    -- Create temporary tables
    CREATE TEMP TABLE TempPerson (
        person_id SERIAL PRIMARY KEY,
        first_name TEXT,
        last_name TEXT,
        email TEXT,
        password_hash BIGINT,
        password_salt BIGINT,
        phone_number TEXT,
        street TEXT,
        city TEXT,
        roles TEXT, -- Load roles as TEXT initially
        salary DECIMAL(19,4),
        country TEXT,
        effective_date TIMESTAMP
    );

    CREATE TEMP TABLE TempOrder (
        order_id SERIAL PRIMARY KEY,
        person_id INT,
        quantity INT,
        product_name TEXT,
        product_cost DECIMAL(19,4),
        product_description TEXT,
        category_name TEXT,
        order_status TEXT,
        payment_method TEXT,
        delivery_method TEXT,
        delivery_cost DECIMAL(19,4),
        created_at TIMESTAMP
    );

    -- Load data into temporary tables
    EXECUTE format('COPY TempPerson (person_id, first_name, last_name, email, password_hash, password_salt, phone_number, roles, salary, country, street, city, effective_date) FROM %L DELIMITER '','' CSV HEADER ENCODING ''LATIN1''', person_file_path);
    EXECUTE format('COPY TempOrder (order_id, person_id, quantity, product_name, product_cost, product_description, category_name, order_status, payment_method, delivery_method, delivery_cost, created_at) FROM %L DELIMITER '','' CSV HEADER ENCODING ''LATIN1''', order_file_path);

    -- Data validation and cleanup
    -- Remove invalid email addresses from TempPerson
    DELETE FROM TempPerson WHERE email !~* '^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$';

    -- Remove orders with invalid product costs or delivery costs or quantities
    DELETE FROM TempOrder WHERE product_cost <= 0;
    DELETE FROM TempOrder WHERE delivery_cost <= 0;
    DELETE FROM TempOrder WHERE quantity <= 0;

    -- Set default values for null fields in TempPerson
    UPDATE TempPerson
    SET first_name = COALESCE(first_name, 'Unknown'),
        last_name = COALESCE(last_name, 'Unknown'),
        email = COALESCE(email, 'noemail@example.com'),
        password_hash = COALESCE(password_hash, 0),
        password_salt = COALESCE(password_salt, 0),
        phone_number = COALESCE(phone_number, '0000000000'),
        street = COALESCE(street, 'Unknown'),
        city = COALESCE(city, 'Unknown'),
        roles = COALESCE(roles, '{}'),
        salary = COALESCE(salary, 0.0),
        country = COALESCE(country, 'Unknown'),
        effective_date = COALESCE(effective_date, NOW());

    -- Set default values for null fields in TempOrder
    UPDATE TempOrder
    SET product_name = COALESCE(product_name, 'Unknown Product'),
        product_description = COALESCE(product_description, 'No description'),
        category_name = COALESCE(category_name, 'Unknown Category'),
        order_status = COALESCE(order_status, 'Pending'),
        payment_method = COALESCE(payment_method, 'Unknown Method'),
        delivery_method = COALESCE(delivery_method, 'Unknown Method'),
        delivery_cost = COALESCE(delivery_cost, 0.0),
        created_at = COALESCE(created_at, NOW());

    -- Convert roles from text to array format
    UPDATE TempPerson
    SET roles = 
        CASE 
            WHEN roles IS NOT NULL THEN 
                replace(replace(roles, '[', '{'), ']', '}')::TEXT[]
            ELSE 
                NULL 
        END;

    -- Insert countries into Country table
    INSERT INTO Country (name)
    SELECT DISTINCT country
    FROM TempPerson
    WHERE country IS NOT NULL
    ON CONFLICT (name) DO NOTHING;

    -- Insert cities into City table
    INSERT INTO City (name, country_id)
    SELECT DISTINCT city, (SELECT id FROM Country WHERE name = TempPerson.country LIMIT 1)
    FROM TempPerson
    WHERE city IS NOT NULL
    ON CONFLICT (name, country_id) DO NOTHING;

    -- Insert addresses into Address table
    INSERT INTO Address (city_id, street)
    SELECT DISTINCT (SELECT id FROM City WHERE name = TempPerson.city LIMIT 1), street
    FROM TempPerson
    WHERE street IS NOT NULL
    ON CONFLICT (city_id, street) DO NOTHING;

    -- Insert persons into Person table
    INSERT INTO Person (id, first_name, last_name, email, password_hash, password_salt, phone_number, address_id)
    SELECT person_id, first_name, last_name, email, password_hash, password_salt, phone_number, 
           (SELECT id FROM Address WHERE city_id = (SELECT id FROM City WHERE name = TempPerson.city LIMIT 1) AND street = TempPerson.street LIMIT 1)
    FROM TempPerson
    ON CONFLICT (email) DO NOTHING;
    
    -- Insert roles into "Role" table
    INSERT INTO "Role" (name)
    SELECT DISTINCT unnest(roles::TEXT[]) AS role_name
    FROM TempPerson
    WHERE roles IS NOT NULL
    ON CONFLICT (name) DO NOTHING;

    -- Insert person roles into PersonRole table
    INSERT INTO PersonRole (person_id, role_id)
    SELECT p.id AS person_id, r.id AS role_id
    FROM TempPerson tp
    JOIN Person p ON tp.email = p.email
    JOIN unnest(tp.roles::TEXT[]) AS role_name ON true
    JOIN "Role" r ON role_name = r.name
    WHERE NOT EXISTS (
        SELECT 1 
        FROM PersonRole pr 
        WHERE pr.person_id = p.id 
        AND pr.role_id = r.id
    );

    -- Insert salaries into Salary table
    INSERT INTO Salary (person_id, amount, effective_date)
    SELECT p.id AS person_id, tp.salary, tp.effective_date
    FROM TempPerson tp
    JOIN Person p ON tp.email = p.email
    WHERE tp.salary IS NOT NULL AND tp.salary > 0
    AND NOT EXISTS (
        SELECT 1 
        FROM Salary s 
        WHERE s.person_id = p.id
    );

    -- Insert categories into Category table
    INSERT INTO Category (name)
    SELECT DISTINCT category_name
    FROM TempOrder
    WHERE category_name IS NOT NULL
    ON CONFLICT (name) DO NOTHING;

    -- Insert products into Product table
    INSERT INTO Product (name, cost, description, category_id)
    SELECT product_name, product_cost, product_description, c.id AS category_id
    FROM TempOrder tord
    JOIN Category c ON tord.category_name = c.name
    ON CONFLICT (name, category_id) DO NOTHING;

    -- Insert payment methods into PaymentMethod table
    INSERT INTO PaymentMethod (name)
    SELECT DISTINCT payment_method
    FROM TempOrder
    WHERE payment_method IS NOT NULL
    ON CONFLICT (name) DO NOTHING;

    -- Insert delivery methods into DeliveryMethod table
    INSERT INTO DeliveryMethod (name, cost)
    SELECT DISTINCT delivery_method, delivery_cost
    FROM TempOrder
    WHERE delivery_method IS NOT NULL AND delivery_cost IS NOT NULL
    ON CONFLICT (name) DO NOTHING;

    -- Insert statuses into Status table
    INSERT INTO Status (name)
    SELECT DISTINCT order_status
    FROM TempOrder
    WHERE order_status IS NOT NULL
    ON CONFLICT (name) DO NOTHING;

    -- Insert orders into "Order" table
    INSERT INTO "Order" (id, person_id, payment_method_id, delivery_method_id, status_id, created_at)
    SELECT order_id, p.id AS person_id, pm.id AS payment_method_id, dm.id AS delivery_method_id, s.id AS status_id, tord.created_at
    FROM TempOrder tord
    JOIN Person p ON tord.person_id = p.id
    JOIN PaymentMethod pm ON tord.payment_method = pm.name
    JOIN DeliveryMethod dm ON tord.delivery_method = dm.name
    JOIN Status s ON tord.order_status = s.name
    WHERE NOT EXISTS (
        SELECT 1 
        FROM "Order" o
        WHERE o.person_id = p.id 
        AND o.payment_method_id = pm.id 
        AND o.delivery_method_id = dm.id 
        AND o.status_id = s.id
        AND o.created_at = tord.created_at
    );
	
	-- Insert product orders into ProductOrder table
	INSERT INTO ProductOrder (product_id, quantity, order_id)
    SELECT pr.id AS product_id, tord.quantity, tord.order_id
    FROM TempOrder tord
    JOIN Product pr ON tord.product_name = pr.name
    WHERE NOT EXISTS (
        SELECT 1
        FROM ProductOrder po
        WHERE po.product_id = pr.id
        AND po.order_id = tord.order_id
    );

    -- Drop temporary tables
    DROP TABLE TempPerson;
    DROP TABLE TempOrder;

END;
$$ LANGUAGE plpgsql;

-- Call the function with appropriate file paths
SELECT load_data_from_csv('D:/Database_project/dataset1/Person.csv', 'D:/Database_project/dataset1/Order.csv');
SELECT load_data_from_csv('D:/Database_project/dataset2/Person2.csv', 'D:/Database_project/dataset2/Order2.csv');

-- Queries to view data
SELECT * FROM Person ORDER BY id;
SELECT * FROM Status ORDER BY id;
SELECT * FROM PaymentMethod ORDER BY id;
SELECT * FROM DeliveryMethod ORDER BY id;
SELECT * FROM "Role" ORDER BY id;
SELECT * FROM Country ORDER BY id;
SELECT * FROM City ORDER BY id;
SELECT * FROM Address ORDER BY id;
SELECT * FROM Category ORDER BY id;
SELECT * FROM PersonRole ORDER BY person_id;
SELECT * FROM Salary ORDER BY person_id;
SELECT * FROM Product ORDER BY id;
SELECT * FROM "Order" ORDER BY person_id;
SELECT * FROM ProductOrder ORDER BY product_order_id;
