-- Drop existing tables
DROP TABLE IF EXISTS Person CASCADE;
DROP TABLE IF EXISTS Salary CASCADE;
DROP TABLE IF EXISTS Country CASCADE;
DROP TABLE IF EXISTS Status CASCADE;
DROP TABLE IF EXISTS "Role" CASCADE;
DROP TABLE IF EXISTS PersonRole CASCADE;
DROP TABLE IF EXISTS Category CASCADE;
DROP TABLE IF EXISTS Product CASCADE;
DROP TABLE IF EXISTS "Order" CASCADE;
DROP TABLE IF EXISTS PaymentMethod CASCADE;
DROP TABLE IF EXISTS DeliveryMethod CASCADE;
DROP TABLE IF EXISTS City CASCADE;
DROP TABLE IF EXISTS Address CASCADE;
DROP TABLE IF EXISTS ProductOrder CASCADE;

-- Create tables
CREATE TABLE Country (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
	CONSTRAINT country_name_unique UNIQUE (name)
);

CREATE TABLE City (
    id SERIAL PRIMARY KEY,
	country_id INT NOT NULL,
    name TEXT NOT NULL,
	FOREIGN KEY (country_id) REFERENCES Country(id),
	CONSTRAINT city_name_country_unique UNIQUE (name, country_id)
);

CREATE TABLE Address (
    id SERIAL PRIMARY KEY,
	city_id INT NOT NULL,
    street TEXT NOT NULL,
	FOREIGN KEY (city_id) REFERENCES City(id),
	CONSTRAINT address_city_street_unique UNIQUE (city_id, street)
);

CREATE TABLE Person (
    id SERIAL PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    email TEXT NOT NULL,
    password_hash BIGINT NOT NULL,
    password_salt BIGINT NOT NULL,
    phone_number TEXT NOT NULL,
    address_id INT NOT NULL,
    FOREIGN KEY (address_id) REFERENCES Address(id),
	CONSTRAINT person_email_unique UNIQUE (email)
);

CREATE TABLE Salary (
    id SERIAL PRIMARY KEY,
    person_id INT NOT NULL,
    amount DECIMAL(19,2) NOT NULL,
    effective_date TIMESTAMP,
    FOREIGN KEY (person_id) REFERENCES Person(id)
);

CREATE TABLE Status (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
	CONSTRAINT status_name_unique UNIQUE (name)
);

CREATE TABLE "Role" (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
	CONSTRAINT role_name_unique UNIQUE (name)
);

CREATE TABLE PersonRole (
    id SERIAL PRIMARY KEY,
    person_id INT NOT NULL,
    role_id INT NOT NULL,
    FOREIGN KEY (person_id) REFERENCES Person(id),
    FOREIGN KEY (role_id) REFERENCES "Role"(id)
);

CREATE TABLE Category (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
	CONSTRAINT category_name_unique UNIQUE (name)
);

CREATE TABLE Product (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    cost DECIMAL(19,2) NOT NULL,
    description TEXT NOT NULL,
    category_id INT NOT NULL,
    FOREIGN KEY (category_id) REFERENCES Category(id),
    CONSTRAINT product_name_category_unique UNIQUE (name, category_id)
);

CREATE TABLE PaymentMethod (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
	CONSTRAINT payment_method_name_unique UNIQUE (name)
);

CREATE TABLE DeliveryMethod (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    cost DECIMAL(19,2) NOT NULL,
	CONSTRAINT delivery_method_name_unique UNIQUE (name)
);

CREATE TABLE "Order" (
    id SERIAL PRIMARY KEY,
    person_id INT NOT NULL,
    payment_method_id INT NOT NULL,
    delivery_method_id INT NOT NULL,
    status_id INT NOT NULL,
	created_at TIMESTAMP NOT NULL,
    FOREIGN KEY (person_id) REFERENCES Person(id),
    FOREIGN KEY (payment_method_id) REFERENCES PaymentMethod(id),
    FOREIGN KEY (delivery_method_id) REFERENCES DeliveryMethod(id),
    FOREIGN KEY (status_id) REFERENCES Status(id)
);

CREATE TABLE ProductOrder (
	product_order_id SERIAL PRIMARY KEY,
	product_id INT NOT NULL,
	quantity INT NOT NULL,
	order_id INT NOT NULL,
	FOREIGN KEY (product_id) REFERENCES Product(id),
	FOREIGN KEY (order_id) REFERENCES "Order"(id)
)
