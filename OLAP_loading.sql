-- Install the required extension
CREATE EXTENSION IF NOT EXISTS postgres_fdw;

-- Create a foreign server that connects to 'datawarehouse'
CREATE SERVER oltp_server
    FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (host 'localhost', dbname 'oltp', port '5432');
    
-- Create a user mapping for the current user
CREATE USER MAPPING FOR CURRENT_USER
    SERVER oltp_server
    OPTIONS (user 'postgres', password 'Lfifrjh,en');

-- Import foreign schema
IMPORT FOREIGN SCHEMA public
FROM SERVER oltp_server
INTO public;

-- DROP USER MAPPING FOR CURRENT_USER SERVER oltp_server;
-- DROP SERVER oltp_server CASCADE;

DROP FUNCTION IF EXISTS transferring_data CASCADE;

-- Function to transfer data from OLTP to OLAP
CREATE OR REPLACE FUNCTION transferring_data()
RETURNS void AS $$
BEGIN
    -- Transferring data to Dim_Location
    INSERT INTO "Dim_Location" ("location_id", "country", "city", "street")
    SELECT a.id AS location_id, c.name AS country, ci.name AS city, a.street
    FROM public.Country c
    JOIN public.City ci ON c.id = ci.country_id
    JOIN public.Address a ON ci.id = a.city_id
    LEFT JOIN "Dim_Location" dl ON c.name = dl."country" AND ci.name = dl."city" AND a.street = dl."street"
    WHERE dl."location_id" IS NULL;

    -- Transferring data to Dim_Person
    INSERT INTO "Dim_Person" ("person_id", "first_name", "last_name", "email", "phone_number", "location_id")
    SELECT p.id AS person_id, p.first_name, p.last_name, p.email, p.phone_number, dl."location_id"
    FROM public.Person p
    JOIN public.Address a ON p.address_id = a.id
    JOIN public.City ci ON a.city_id = ci.id
    JOIN public.Country c ON ci.country_id = c.id
    JOIN "Dim_Location" dl ON c.name = dl."country" AND ci.name = dl."city" AND a.street = dl."street"
    LEFT JOIN "Dim_Person" dp ON p.email = dp."email"
    WHERE dp."person_id" IS NULL AND p.email IS NOT NULL AND p.phone_number IS NOT NULL;

    -- Transferring data to Dim_Role
    INSERT INTO "Dim_Role" ("role_id", "name")
    SELECT r.id AS role_id, r.name
    FROM public."Role" r
    LEFT JOIN "Dim_Role" dr ON r.name = dr."name"
    WHERE dr."role_id" IS NULL AND r.name IS NOT NULL;

    -- Transferring data to Dim_PersonRole
    INSERT INTO "Dim_PersonRole" ("person_role_id", "person_id", "role_id")
    SELECT pr.id AS person_role_id, p.id AS person_id, r.id AS role_id
    FROM public.PersonRole pr
    JOIN public.Person p ON pr.person_id = p.id
    JOIN public."Role" r ON pr.role_id = r.id
    JOIN "Dim_Person" dp ON p.email = dp."email"
    JOIN "Dim_Role" dr ON r.name = dr."name"
    LEFT JOIN "Dim_PersonRole" dpr ON dp."person_id" = dpr."person_id" AND dr."role_id" = dpr."role_id"
    WHERE dpr."person_role_id" IS NULL;

    -- Transferring data to Dim_Category
    INSERT INTO "Dim_Category" ("category_id", "name")
    SELECT c.id AS category_id, c.name
    FROM public.Category c
    LEFT JOIN "Dim_Category" dc ON c.name = dc."name"
    WHERE dc."category_id" IS NULL AND c.name IS NOT NULL;

    -- Transferring data to Dim_Product
    INSERT INTO "Dim_Product" ("product_id", "name", "description", "category_id", "cost")
    SELECT p.id AS product_id, p.name, p.description, dc."category_id", p.cost
    FROM public.Product p
    JOIN public.Category c ON p.category_id = c.id
    JOIN "Dim_Category" dc ON c.name = dc."name"
    LEFT JOIN "Dim_Product" dp ON p.name = dp."name" AND dc."category_id" = dp."category_id"
    WHERE dp."product_id" IS NULL AND p.name IS NOT NULL AND p.cost >= 0;

    -- Transferring data to Dim_PaymentMethod
    INSERT INTO "Dim_PaymentMethod" ("payment_method_id", "name")
    SELECT pm.id AS payment_method_id, pm.name
    FROM public.PaymentMethod pm
    LEFT JOIN "Dim_PaymentMethod" dpm ON pm.name = dpm."name"
    WHERE dpm."payment_method_id" IS NULL AND pm.name IS NOT NULL;

    -- Transferring data to Dim_DeliveryMethod
    INSERT INTO "Dim_DeliveryMethod" ("delivery_method_id", "name", "cost")
    SELECT dm.id AS delivery_method_id, dm.name, dm.cost
    FROM public.DeliveryMethod dm
    LEFT JOIN "Dim_DeliveryMethod" ddm ON dm.name = ddm."name"
    WHERE ddm."delivery_method_id" IS NULL AND dm.name IS NOT NULL AND dm.cost >= 0;

    -- Transferring data to Dim_Status
    INSERT INTO "Dim_Status" ("status_id", "name")
    SELECT s.id AS status_id, s.name
    FROM public.Status s
    LEFT JOIN "Dim_Status" ds ON s.name = ds."name"
    WHERE ds."status_id" IS NULL AND s.name IS NOT NULL;
	
	-- Transferring data to Dim_Time from Order.created_at
    INSERT INTO "Dim_Time" ("date", "day", "month", "quarter", "year")
    SELECT DISTINCT DATE(o.created_at) AS date,
	 	EXTRACT(DAY FROM o.created_at) AS day,
        EXTRACT(MONTH FROM o.created_at) AS month,
        EXTRACT(QUARTER FROM o.created_at) AS quarter,
        EXTRACT(YEAR FROM o.created_at) AS year
    FROM public."Order" o
    LEFT JOIN "Dim_Time" dt ON DATE(o.created_at) = dt."date"
    WHERE dt."date" IS NULL AND o.created_at > (CURRENT_DATE - INTERVAL '12 months');

    -- Transferring data to Dim_Time from Salary.effective_date
    INSERT INTO "Dim_Time" ("date", "day", "month", "quarter", "year")
    SELECT DISTINCT DATE(s.effective_date) AS date,
	 	EXTRACT(DAY FROM s.effective_date) AS day,
        EXTRACT(MONTH FROM s.effective_date) AS month,
        EXTRACT(QUARTER FROM s.effective_date) AS quarter,
        EXTRACT(YEAR FROM s.effective_date) AS year
    FROM public.Salary s
    LEFT JOIN "Dim_Time" dt ON DATE(s.effective_date) = dt."date"
    WHERE dt."date" IS NULL AND s.effective_date > (CURRENT_DATE - INTERVAL '12 months');
	
    -- Transferring data to Fact_Salary
    INSERT INTO "Fact_Salary" ("person_id", "date_id", "amount")
    SELECT dp."person_id", dt."date_id", s.amount
    FROM public.Salary s
    JOIN public.Person p ON s.person_id = p.id
    JOIN "Dim_Person" dp ON p.email = dp."email"
    JOIN "Dim_Time" dt ON DATE(s.effective_date) = dt."date"
    LEFT JOIN "Fact_Salary" fs ON dp."person_id" = fs."person_id" AND dt."date_id" = fs."date_id"
    WHERE fs."person_id" IS NULL AND s.amount >= 0;
	
    -- Transferring data to Dim_Order
    -- Transferring data to Dim_Order
    INSERT INTO "Dim_Order" ("order_id", "person_id", "date_id", "payment_method_id", "delivery_method_id", "status_id")
    SELECT o.id AS order_id, dp."person_id", dt."date_id", dpm."payment_method_id", ddm."delivery_method_id", ds."status_id"
    FROM public."Order" o
    JOIN public.Person p ON o.person_id = p.id
    JOIN public.PaymentMethod pm ON o.payment_method_id = pm.id
    JOIN public.DeliveryMethod dm ON o.delivery_method_id = dm.id
    JOIN public.Status s ON o.status_id = s.id
    JOIN "Dim_Person" dp ON p.email = dp."email"
    JOIN "Dim_PaymentMethod" dpm ON pm.name = dpm."name"
    JOIN "Dim_DeliveryMethod" ddm ON dm.name = ddm."name"
    JOIN "Dim_Status" ds ON s.name = ds."name"
    JOIN "Dim_Time" dt ON DATE(o.created_at) = dt."date"
    LEFT JOIN "Dim_Order" dio ON o.id = dio."order_id"
    WHERE dio."order_id" IS NULL;
	
    -- Transferring data to Fact_Sales
    INSERT INTO "Fact_Sales" ("order_id", "product_id", "quantity_sold", "total_sales")
    SELECT po.order_id AS order_id, po.product_id AS product_id, po.quantity AS quantity_sold, (po.quantity * p.cost) AS total_sales
    FROM public.ProductOrder po
    JOIN public."Order" o ON po.order_id = o.id
    JOIN public.Product p ON po.product_id = p.id
    LEFT JOIN "Fact_Sales" fs ON po.order_id = fs."order_id" AND po.product_id = fs."product_id"
    WHERE fs."product_sales_id" IS NULL AND po.quantity > 0;
END;
$$ LANGUAGE plpgsql;

-- Call the function
SELECT transferring_data();

-- Проверка данных в таблицах OLAP
SELECT * FROM "Dim_Location" ORDER BY "location_id";
SELECT * FROM "Dim_Person" ORDER BY "person_id";
SELECT * FROM "Dim_Category" ORDER BY "category_id";
SELECT * FROM "Dim_PaymentMethod" ORDER BY "payment_method_id";
SELECT * FROM "Dim_DeliveryMethod" ORDER BY "delivery_method_id";
SELECT * FROM "Fact_Salary" ORDER BY "salary_id";
SELECT * FROM "Fact_Sales" ORDER BY "product_sales_id";
SELECT * FROM "Dim_Role" ORDER BY "role_id";
SELECT * FROM "Dim_PersonRole" ORDER BY "person_role_id";
SELECT * FROM "Dim_Product" ORDER BY "product_id";
SELECT * FROM "Dim_Status" ORDER BY "status_id";
SELECT * FROM "Dim_Time";
SELECT * FROM "Dim_Order" ORDER BY "order_id";
