-- Drop foreign key constraints on product_id in Fact_Sales
ALTER TABLE "Fact_Sales" DROP CONSTRAINT IF EXISTS "Fact_Sales_product_id_fkey";

-- Remove the existing primary key constraint from Dim_Product
ALTER TABLE "Dim_Product" DROP CONSTRAINT IF EXISTS "Dim_Product_pkey" CASCADE;

-- Add new columns for SCD Type 2
ALTER TABLE "Dim_Product"
ADD COLUMN "productHistory_ID" SERIAL PRIMARY KEY,
ADD COLUMN "start_date" TIMESTAMP,
ADD COLUMN "end_date" TIMESTAMP,
ADD COLUMN "current_flag" BOOLEAN DEFAULT TRUE;

UPDATE "Dim_Product"
SET "start_date" = NOW(),
    "end_date" = '9999-12-31',
    "current_flag" = TRUE;

DROP FUNCTION IF EXISTS DimProduct_update_trigger CASCADE;

CREATE OR REPLACE FUNCTION DimProduct_update_trigger()
RETURNS TRIGGER AS $$
BEGIN
    -- Check if any relevant field is being updated
    IF (OLD.name <> NEW.name OR
        OLD.description <> NEW.description OR
        OLD.category_id <> NEW.category_id OR
        OLD.cost <> NEW.cost) AND OLD.current_flag AND NEW.current_flag THEN
        
        -- Set the end_date of the old record
        UPDATE "Dim_Product"
        SET "end_date" = CURRENT_TIMESTAMP,
            "current_flag" = FALSE
        WHERE "product_id" = OLD."product_id"
          AND "current_flag" = TRUE;

        -- Insert a new record with updated values
        INSERT INTO "Dim_Product" (
            "product_id", "name", "description", "category_id", "cost", "start_date", "end_date", "current_flag"
        )
        VALUES (
            OLD."product_id", NEW."name", NEW."description", NEW."category_id", NEW."cost", CURRENT_TIMESTAMP, '9999-12-31', TRUE
        );
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS DimProduct_update ON "Dim_Product" CASCADE;

CREATE TRIGGER DimProduct_update
AFTER UPDATE ON "Dim_Product"
FOR EACH ROW
EXECUTE FUNCTION DimProduct_update_trigger();

-- Example update to Dim_Product to trigger the SCD Type 2 logic
UPDATE "Dim_Product"
SET
    "name" = 'New Product Name',
    "description" = 'Updated description',
    "category_id" = (SELECT "category_id" FROM "Dim_Category" WHERE name = 'Networking'),
    "cost" = 49.99
WHERE
    "product_id" = (SELECT "product_id" FROM "Dim_Product" WHERE name = 'Netgear Nighthawk X10')
    AND "current_flag" = TRUE;
