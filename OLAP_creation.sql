-- Drop existing tables if they exist
DROP TABLE IF EXISTS "Dim_Location" CASCADE;
DROP TABLE IF EXISTS "Dim_Person" CASCADE;
DROP TABLE IF EXISTS "Dim_Role" CASCADE;
DROP TABLE IF EXISTS "Dim_PersonRole" CASCADE;
DROP TABLE IF EXISTS "Dim_Product" CASCADE;
DROP TABLE IF EXISTS "Dim_Category" CASCADE;
DROP TABLE IF EXISTS "Dim_Time" CASCADE;
DROP TABLE IF EXISTS "Dim_PaymentMethod" CASCADE;
DROP TABLE IF EXISTS "Dim_DeliveryMethod" CASCADE;
DROP TABLE IF EXISTS "Dim_Status" CASCADE;
DROP TABLE IF EXISTS "Fact_Salary" CASCADE;
DROP TABLE IF EXISTS "Fact_Sales" CASCADE;
DROP TABLE IF EXISTS "Dim_Order" CASCADE;

-- Create tables
CREATE TABLE "Dim_Location" (
  "location_id" SERIAL PRIMARY KEY,
  "country" TEXT NOT NULL,
  "city" TEXT NOT NULL,
  "street" TEXT NOT NULL
);

CREATE TABLE "Dim_Person" (
  "person_id" SERIAL PRIMARY KEY,
  "first_name" TEXT NOT NULL,
  "last_name" TEXT NOT NULL,
  "email" TEXT NOT NULL,
  "phone_number" TEXT NOT NULL,
  "location_id" INT NOT NULL
);

CREATE TABLE "Dim_Role" (
  "role_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL
);

CREATE TABLE "Dim_PersonRole" (
  "person_role_id" SERIAL PRIMARY KEY,
  "person_id" INT NOT NULL,
  "role_id" INT NOT NULL
);

CREATE TABLE "Dim_Category" (
  "category_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL
);

CREATE TABLE "Dim_Product" (
  "product_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "description" TEXT NOT NULL,
  "category_id" INT NOT NULL,
  "cost" DECIMAL(19,2) NOT NULL
);

CREATE TABLE "Dim_Time" (
  "date_id" SERIAL PRIMARY KEY,
  "date" DATE NOT NULL,
  "day" INT NOT NULL,
  "month" INT NOT NULL,
  "quarter" INT NOT NULL,
  "year" INT NOT NULL
);

CREATE TABLE "Dim_PaymentMethod" (
  "payment_method_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL
);

CREATE TABLE "Dim_DeliveryMethod" (
  "delivery_method_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
  "cost" DECIMAL(19,2) NOT NULL
);

CREATE TABLE "Dim_Status" (
  "status_id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL
);

CREATE TABLE "Fact_Salary" (
  "salary_id" SERIAL PRIMARY KEY,
  "person_id" INT NOT NULL,
  "date_id" INT NOT NULL,
  "amount" DECIMAL(19,2) NOT NULL
);

CREATE TABLE "Dim_Order" (
  "order_id" SERIAL PRIMARY KEY,
  "person_id" INT NOT NULL,
  "date_id" INT NOT NULL,
  "payment_method_id" INT NOT NULL,
  "delivery_method_id" INT NOT NULL,
  "status_id" INT NOT NULL
);

CREATE TABLE "Fact_Sales" (
  "product_sales_id" SERIAL PRIMARY KEY,
  "order_id" INT NOT NULL,
  "product_id" INT NOT NULL,
  "quantity_sold" INT NOT NULL,
  "total_sales" DECIMAL(19,2) NOT NULL
);

-- Add foreign key constraints
ALTER TABLE "Dim_Person" ADD FOREIGN KEY ("location_id") REFERENCES "Dim_Location" ("location_id");

ALTER TABLE "Dim_PersonRole" ADD FOREIGN KEY ("role_id") REFERENCES "Dim_Role" ("role_id");

ALTER TABLE "Dim_PersonRole" ADD FOREIGN KEY ("person_id") REFERENCES "Dim_Person" ("person_id");

ALTER TABLE "Dim_Product" ADD FOREIGN KEY ("category_id") REFERENCES "Dim_Category" ("category_id");

ALTER TABLE "Fact_Salary" ADD FOREIGN KEY ("person_id") REFERENCES "Dim_Person" ("person_id");

ALTER TABLE "Fact_Salary" ADD FOREIGN KEY ("date_id") REFERENCES "Dim_Time" ("date_id");

ALTER TABLE "Dim_Order" ADD FOREIGN KEY ("person_id") REFERENCES "Dim_Person" ("person_id");

ALTER TABLE "Dim_Order" ADD FOREIGN KEY ("date_id") REFERENCES "Dim_Time" ("date_id");

ALTER TABLE "Dim_Order" ADD FOREIGN KEY ("payment_method_id") REFERENCES "Dim_PaymentMethod" ("payment_method_id");

ALTER TABLE "Dim_Order" ADD FOREIGN KEY ("delivery_method_id") REFERENCES "Dim_DeliveryMethod" ("delivery_method_id");

ALTER TABLE "Dim_Order" ADD FOREIGN KEY ("status_id") REFERENCES "Dim_Status" ("status_id");

ALTER TABLE "Fact_Sales" ADD FOREIGN KEY ("order_id") REFERENCES "Dim_Order" ("order_id");

ALTER TABLE "Fact_Sales" ADD FOREIGN KEY ("product_id") REFERENCES "Dim_Product" ("product_id");

-- Indexes on Foreign Keys
CREATE INDEX idx_person_location_id ON "Dim_Person" ("location_id");
CREATE INDEX idx_personrole_role_id ON "Dim_PersonRole" ("role_id");
CREATE INDEX idx_personrole_person_id ON "Dim_PersonRole" ("person_id");
CREATE INDEX idx_product_category_id ON "Dim_Product" ("category_id");
CREATE INDEX idx_salary_person_id ON "Fact_Salary" ("person_id");
CREATE INDEX idx_salary_date_id ON "Fact_Salary" ("date_id");
CREATE INDEX idx_order_person_id ON "Dim_Order" ("person_id");
CREATE INDEX idx_order_date_id ON "Dim_Order" ("date_id");
CREATE INDEX idx_order_payment_method_id ON "Dim_Order" ("payment_method_id");
CREATE INDEX idx_order_delivery_method_id ON "Dim_Order" ("delivery_method_id");
CREATE INDEX idx_order_status_id ON "Dim_Order" ("status_id");